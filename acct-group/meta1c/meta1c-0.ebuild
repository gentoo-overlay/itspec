# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-group

DESCRIPTION="SMTPC group for mail-mta/meta1"
ACCT_GROUP_ID=-1
