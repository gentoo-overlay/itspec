# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="QMGR user for mail-mta/meta1"
ACCT_USER_ID=-1
ACCT_USER_GROUPS=( meta1q meta1m )

acct-user_add_deps
