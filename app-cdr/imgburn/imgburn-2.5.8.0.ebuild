# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop

IUSE="cdi"
DESCRIPTION="ImgBurn is a lightweight CD/DVD/HD DVD/Blu-ray burning application that everyone should have in their toolkit!"
HOMEPAGE="http://www.imgburn.com/"
SRC_URI="http://download.imgburn.com/SetupImgBurn_2.5.8.0.exe
        cdi? ( http://download.imgburn.com/pfctoc.zip )"

use_prepare() {
    local file="${EBUILD%/*}/files/${PN}-langs.txt"
    local line
    local lang
    local link

    while read line; do
        lang="${line%% *}"
        link="${line#* }"
        [[ ${lang} != "en" ]] && SRC_URI="${SRC_URI} l10n_${lang}? ( ${link} -> ${PN}-${lang}.zip )"
        IUSE+=" l10n_${lang}"
    done < "${file}"
}

use_prepare

LICENSE="freeware"
RESTRICT="mirror"

SLOT="0"
KEYWORDS="~x86 ~amd64"

DEPEND="app-arch/p7zip
        media-gfx/icoutils
        media-gfx/imagemagick"

RDEPEND="virtual/wine"

S="${WORKDIR}"

src_unpack() {
    local lang

    cd "${DISTDIR}"
    7z x -o"${S}" SetupImgBurn_"${PV}".exe || die "SetupImgBurn_"${PV}".exe unpack failed"

    if use cdi; then
        7z x -o"${S}" pfctoc.zip || die "pfctoc.zip unpack failed"
    fi

    mkdir "${S}/Languages"
    for lang in ${L10N}; do
        if [[ ${lang} != "en" ]] ; then
            7z x -o"${S}/Languages" ${PN}-${lang}.zip || die "${PN}-${lang}.zip unpack failed"
        fi
    done
}

src_prepare() {
    default
    cd "${S}"
    rm -r "\$PLUGINSDIR"
}

src_install() {
    insinto /opt/${PN}
    doins -r * || die

    bash "${FILESDIR}"/exe2png "ImgBurn.exe" "imgburn_256.png" "256x256"
    for path in $(find /usr/share/icons/hicolor -maxdepth 1 -type d -iname '[0-9]*x[0-9]*'); do
        size=$(basename "${path}")
        convert imgburn_256.png -resize "${size}" imgburn.png
        insinto "${path}"/apps
        doins imgburn.png
    done

    exeinto /opt/bin
    doexe "${FILESDIR}/${PN}" || die

    make_desktop_entry imgburn "ImgBurn" imgburn "AudioVideo;DiscBurning;System;Filesystem;" || die
}
