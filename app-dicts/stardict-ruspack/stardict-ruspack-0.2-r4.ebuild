# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Collection of dicts for stardict."
HOMEPAGE="http://gnome.msiu.ru/stardict.php"
SRC_URI="https://bitbucket.org/gentoo-overlay/itspec/downloads/${PN}-${PV}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="!app-dicts/stardict-freedict-eng-rus"

DEPEND=""

S="${WORKDIR}"

src_install() {
	insinto /usr/share/stardict/dic
	doins *
}
