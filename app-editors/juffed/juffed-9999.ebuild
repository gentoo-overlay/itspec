# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake git-r3 xdg-utils

DESCRIPTION="QScintilla-based tabbed text editor with syntax highlighting"
HOMEPAGE="http://juffed.com/en/ https://github.com/Mezomish/juffed"
EGIT_REPO_URI="https://github.com/Mezomish/juffed.git"
EGIT_BRANCH="master"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="debug"

RDEPEND="
	app-i18n/enca
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtprintsupport:5
	dev-qt/qtsingleapplication[qt5(+),X]
	dev-qt/qtwidgets:5
	dev-qt/qtxml:5
	>=x11-libs/qscintilla-2.9.4:=[qt5(+)]
"
DEPEND="${RDEPEND}"
BDEPEND="dev-qt/linguist-tools:5"

DOCS=( ChangeLog README )

src_prepare() {
	# Upstream version outdated/dysfunctional and CRLF terminated
	cp "${FILESDIR}"/FindQtSingleApplication.cmake cmake/ || die

	cmake_src_prepare

	sed -i -e '/set(CMAKE_CXX_FLAGS/d' CMakeLists.txt || die

	eapply_user
}

src_configure() {
	local libdir=$(get_libdir)
	local mycmakeargs=(
		-DUSE_ENCA=ON
		-DUSE_QT5=ON
		-DUSE_SYSTEM_QTSINGLEAPPLICATION=ON
		-DLIB_SUFFIX=${libdir/lib/}
		-DQSCINTILLA_NAMES="qscintilla2;libqscintilla2;qscintilla2_qt5;qscintilla2_qt5"
	)
	cmake_src_configure
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}
