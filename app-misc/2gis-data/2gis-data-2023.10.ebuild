# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Proprietary freeware multimedia map of several Russian and Ukrainian towns (data)"
HOMEPAGE="http://2gis.ru"

LICENSE="2Gis-ru"
RESTRICT="mirror"

SLOT="0"
KEYWORDS="~x86 ~amd64"

DEPEND="app-arch/unzip"
RDEPEND=">=app-misc/2gis-3.16.3.0"

# filled in 'use_prepare' from file
IUSE=""
SRC_URI=""

S="${WORKDIR}"

use_prepare() {
    local file="${EBUILD%/*}/files/use-links-${PV}.txt"
    local line
    local useflag
    local link

    while read line; do
        useflag="${line%% *}"
        link="${line#* }"
        SRC_URI="${SRC_URI} ${useflag}? ( ${link} )"
        IUSE="${IUSE} ${useflag}"
    done < "${file}"
}

use_prepare

src_install() {
    insinto /opt/2gis
    # Only attempt to install any data if the user has enabled at least
    # one useflag.
    if [ -d 2gis/3.0 ]; then
        # Only required data files were unpacked, so it should be safe
        # to use wildcard.
        doins -r 2gis/3.0/* || die
    fi
}
