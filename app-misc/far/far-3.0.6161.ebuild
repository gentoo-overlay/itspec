# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker desktop

DESCRIPTION="File and archive manager"
HOMEPAGE="http://farmanager.com/"
BUILD_DATE="20230609"
BUILD_VER="${PV/./}"
BUILD_MAJOR_VER="${BUILD_VER:0:2}"
BUILD_MINOR_VER="${BUILD_VER:3:4}"
SRC_URI="x86_32? ( http://farmanager.com/files/Far${BUILD_MAJOR_VER}b${BUILD_MINOR_VER}.x86.${BUILD_DATE}.7z )
         x86_64? ( http://farmanager.com/files/Far${BUILD_MAJOR_VER}b${BUILD_MINOR_VER}.x64.${BUILD_DATE}.7z )"

LICENSE="FarManager"
RESTRICT="mirror"

SLOT="0"
IUSE="x86_32 x86_64"
REQUIRED_USE="|| ( x86_32 x86_64 )"

KEYWORDS="~x86 ~amd64"

DEPEND="app-arch/p7zip
        media-gfx/icoutils
        media-gfx/imagemagick"

RDEPEND="virtual/wine"

S="${WORKDIR}"

src_install() {
    insinto /opt/${PN}
    doins -r * || die

    for path in $(find /usr/share/icons/hicolor -maxdepth 1 -type d -iname '[0-9]*x[0-9]*'); do
        size=$(basename "${path}")
        convert ${FILESDIR}/Far_17_256x256x32.png -resize "${size}" far.png
        insinto "${path}"/apps
        doins far.png
    done

    exeinto /opt/bin
    doexe "${FILESDIR}/${PN}" || die

    make_desktop_entry far "Far manager" far "System;Filesystem;" || die
}
