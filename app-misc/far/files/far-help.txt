Far Manager, version 3.0 (build 3800) x86
Copyright © 1996-2000 Eugene Roshal, Copyright © 2000-2014 Far Group
Usage: far [switches] [apath [ppath]]

where
  apath - path to a folder (or a file or an archive or command with prefix)
          for the active panel
  ppath - path to a folder (or a file or an archive or command with prefix)
          for the passive panel

The following switches may be used in the command line:

 /?   This help.
 /a   Disable display of characters with codes 0 - 31 and 255.
 /ag  Disable display of pseudographics characters.
 /co  Forces FAR to load plugins from the cache only.
 /e[<line>[:<pos>]] <filename>
      Edit the specified file.
 /m   Do not load macros.
 /ma  Do not execute auto run macros.
 /p[<path>]
      Search for "common" plugins in the directory, specified by <path>.
 /s <profilepath> [<localprofilepath>]
      Custom location for Far configuration files - overrides Far.exe.ini.
 /t <path>
      Location of Far template configuration file - overrides Far.exe.ini.
 /u <username>
      Allows to have separate registry settings for different users.
      Affects only 1.x Far Manager plugins
 /v <filename>
      View the specified file. If <filename> is -, data is read from the stdin.
 /w[-] Stretch to console window instead of console buffer or vise versa.
 /clearcache [profilepath [localprofilepath]]
      Clear plugins cache.
 /export <out.farconfig> [profilepath [localprofilepath]]
      Export settings.
 /import <in.farconfig> [profilepath [localprofilepath]]
      Import settings.
 /ro  Read-Only config mode.
 /rw  Normal config mode.
