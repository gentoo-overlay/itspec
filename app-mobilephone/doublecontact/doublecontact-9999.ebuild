# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 qmake-utils

DESCRIPTION="Offline DE-independent contact manager primarily for phonebooks editing/merging"
HOMEPAGE="http://zvyozdochkin.ru/projects/doublecontact/"
EGIT_REPO_URI="https://github.com/DarkHobbit/doublecontact/"
EGIT_BRANCH="master"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	dev-qt/qtxml:5
"

RDEPEND="${DEPEND}"

DOCS=( doc/changelog  doc/changelog.rus  doc/csvprofiles.en.md  doc/manual.en.md  doc/manual.rus.md  README.md )

src_prepare() {
	LRELEASE="$(qt5_get_bindir)/lrelease"
	sed -e "s:lrelease-qt5:${LRELEASE}:" -i "${S}"/app/doublecontact.pro
	default
}

src_configure() {
	eqmake5 all.pro
}

src_install() {
	einstalldocs

	insinto "/usr/share/icons/hicolor"
	for x in 128x128  16x16  256x256  32x32  512x512  64x64 ; do
		doins -r img/${x} || die
	done

	insinto "/usr/share/doublecontact/translations"
	doins translations/*.qm || die
	doins translations/*.utf8 || die

	insinto "/usr/share/applications"
	doins ${FILESDIR}/doublecontact.desktop || die

	dobin app/doublecontact
	dobin contconv/contconv
}
