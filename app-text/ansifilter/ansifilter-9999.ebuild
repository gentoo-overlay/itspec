# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs qmake-utils git-r3

DESCRIPTION="Handles text files containing ANSI terminal escape codes"
HOMEPAGE="http://www.andre-simon.de/"
EGIT_REPO_URI="https://gitlab.com/saalen/ansifilter.git"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS=""
IUSE="qt5"

RDEPEND="
	qt5? (
		dev-qt/qtcore:5
		dev-qt/qtgui:5
		dev-qt/qtwidgets:5
	)"
DEPEND="${RDEPEND}"

src_prepare() {
	default

	# bug 431452
	rm src/qt-gui/moc_mydialog.cpp || die
}

src_configure() {
	if use qt5 ; then
		pushd src/qt-gui > /dev/null || die
		eqmake5
		popd > /dev/null || die
	fi
}

src_compile() {
	emake -f makefile CC="$(tc-getCXX)" CXXFLAGS="${CXXFLAGS} -DNDEBUG -std=c++11"

	if use qt5 ; then
		pushd src/qt-gui > /dev/null || die
		emake
		popd > /dev/null || die
	fi
}

src_install() {
	dobin src/${PN}
	use qt5 && dobin src/qt-gui/${PN}-gui

	gunzip man/${PN}.1.gz
	doman man/${PN}.1
	einstalldocs
}
