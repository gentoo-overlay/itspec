# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

FONT_PN="urwfonts"

inherit autotools font

DESCRIPTION="GhostPCL, GhostXPS, Ghostscript and GhostPDF Interpreters"
HOMEPAGE="https://ghostscript.readthedocs.io/en/latest/Readme.html#ghostpdl"
SRC_URI="https://github.com/ArtifexSoftware/${PN}-downloads/releases/download/gs${PV//.}/${PN}-${PV}.tar.xz"

LICENSE="AGPL-3"

RESTRICT="mirror"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+pdl +pcl +xps +tools doc fonts sse2 X"

DEPEND="fonts? ( media-fonts/urwfonts )
        X? ( x11-libs/libXt x11-libs/libXext )"

RDEPEND="${DEPEND}"

src_prepare() {
    default
    if use fonts; then
        sed -e "s:/windows/fonts:"${FONTDIR}";/windows/fonts:" -i ${S}/pcl/pl/pjparse.c
        sed -e "s:/windows/fonts:"${FONTDIR}":" -i ${S}/pcl/tools/makeromttf.py
    fi
    cd "${S}" || die
    eautoreconf

    cd "${S}/ijs" || die
    eautoreconf
}

src_configure() {
        econf \
        $(use_enable sse2) \
        $(use_with X x)
}

src_compile() {
    # -j1 needed because of bug #550926
    emake -j1
}

src_install() {
    if use pdl; then
        dobin "${S}/bin/gpdl" || die "gpdl install failed"
    fi

    if use pcl; then
        dobin "${S}/bin/gpcl6" || die "gpcl6 install failed"
    fi

    if use xps; then
        dobin "${S}/bin/gxps" || die "gxps install failed"
    fi

    if use tools; then
        dobin "${S}/pcl/tools/pcl2pdf" || die "pcl2pdf install failed"
        dobin "${S}/pcl/tools/pcl2pdfwr" || die "pcl2pdfwr install failed"
    fi

    if use doc; then
        dodoc "${S}/doc/pclxps/${PN}".{pdf,txt,tex} || die "doc install failed"
    fi
}
