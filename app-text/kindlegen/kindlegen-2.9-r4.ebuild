# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit edos2unix

KG_PV="${PV/./_}"

# last available build
KG_BUILD="1028-0897292"

DESCRIPTION="Amazon's converter their HTML, XHTML, XML (OPF/IDPF format), or ePub source into a Kindle Book"
HOMEPAGE="http://www.amazon.com/gp/feature.html?docId=1000765211"

# http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz is no longer available
SRC_URI="https://bitbucket.org/gentoo-overlay/itspec/downloads/kindlegen_linux_2.6_i386_v${KG_PV}-${KG_BUILD}.tar.gz"

SLOT="0"
LICENSE="KindleGen-EULA"
RESTRICT="mirror"

KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND=""
DEPEND=""

S="${WORKDIR}"

RESTRICT="strip mirror"

rdos2unix() {
    find . -name '*'.$1 -type f -print0 | while read -d $'\0' file
    do
        edos2unix "$file" || die "Failed to convert line-endings of all .$1 files";
    done
}

src_prepare() {
    default
    rdos2unix txt
    rdos2unix html
}

src_install() {
    exeinto /opt/bin
    doexe kindlegen || die
    dodoc EULA.txt "KindleGen Legal Notices 2013-02-19 Linux.txt" docs/english/*.*
}
