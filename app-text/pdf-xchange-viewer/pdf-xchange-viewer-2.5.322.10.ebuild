# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop

DESCRIPTION="Proprietary smallest, fastest, most feature-rich PDF reader/viewer using wine"
HOMEPAGE="https://www.tracker-software.com/product/pdf-xchange-viewer"
SRC_URI="https://downloads.pdf-xchange.com/PDFX_Vwr_Port.zip -> PDFX_Vwr_Port-${PV}.zip
         ocr? ( https://downloads.pdf-xchange.com/PDFX_Vwr_Port_OCR.zip -> PDFX_Vwr_Port_OCR-${PV}.zip )
         "
         #http://downloads.pdf-xchange.com/TSP_Langtl.zip -> TSP_Langtl-${PV}.zip

#The FREE PDF-XChange Viewer and features are free for private and commercial use - provided it is not bundled with other software and/or distributed for financial gain.
LICENSE="PDF-XChange PDF Viewer/Editor EULA Versions 1-5.x."
RESTRICT="mirror"

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="ocr"

DEPEND="app-arch/unzip
        media-gfx/icoutils
        media-gfx/imagemagick"

RDEPEND="virtual/wine"

S="${WORKDIR}"

src_install() {
    (
    insinto "/opt/${PN}"
    doins -r *
    ) || die

    bash "${FILESDIR}"/exe2png "PDFXCview.exe" "pdf-xchange-viewer_256.png" "256x256"
    for path in $(find /usr/share/icons/hicolor -maxdepth 1 -type d -iname '[0-9]*x[0-9]*'); do
        size=$(basename "${path}")
        convert pdf-xchange-viewer_256.png -resize "${size}" pdf-xchange-viewer.png
            (
            insinto "${path}"/apps
            doins pdf-xchange-viewer.png
            ) || die
    done

    (
    exeopts -m 0755
    exeinto "/opt/bin"
    newexe "${FILESDIR}/${PN}" "${PN}"
    ) || die

    newmenu "${FILESDIR}/icon/${PN}.desktop" "${PN}.desktop" || die
}
