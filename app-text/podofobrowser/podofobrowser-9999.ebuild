# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 desktop

DESCRIPTION="PoDoFoBrowser is a Qt application for browsing the objects in a PDF file and modifying their keys easily."
HOMEPAGE="http://podofo.sourceforge.net/index.html"
EGIT_REPO_URI="https://github.com/KubaO/podofobrowser.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-text/podofo
        dev-qt/qtcore:4
        >=dev-util/cmake-2.4.4"

RDEPEND=""

src_compile() {

    cmake -G "Unix Makefiles" \
        -DLIBPODOFO_DIR=/usr \
        -DCMAKE_INCLUDE_PATH=/usr/include \
        -DCMAKE_LIBRARY_PATH=/usr/lib \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DLIBFREETYPE_FT2BUILD_H=/usr/include/freetype2 \
        -DLIBFREETYPE_FTHEADER_H=/usr/include/freetype2/config \
        -DCMAKE_C_FLAGS="${CFLAGS}" \
        || die "cmake failed"
    make || die "compile failed"
}

src_install() {
    emake DESTDIR=${D} install || die 'Install Failed'
    dodoc AUTHORS INSTALL README TODO
    newicon "${PN}.svg" "${PN}.svg"
    make_desktop_entry ${PN} "PoDoFoBrowser" ${PN} "Graphics" || die "desktop file sed failed"
}
