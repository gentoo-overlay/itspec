# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Converts XML/HTML to PDF."

HOMEPAGE="http://www.princexml.com/"

SRC_URI="x86? ( http://www.princexml.com/download/${PN}-${PV}-linux-generic-i686.tar.gz )
         amd64? ( http://www.princexml.com/download/${PN}-${PV}-linux-generic-x86_64.tar.gz )"

LICENSE="Prince-EULA"
RESTRICT="mirror"
SLOT="0"
IUSE=""
KEYWORDS="~x86 ~amd64"

DEPEND=""

RDEPEND=">=app-arch/bzip2-1.0.6
         >=dev-libs/expat-2.1.0
         >=dev-libs/libxml2-2.9.2
         >=media-libs/fontconfig-2.11.1
         >=media-libs/freetype-2.5.5
         >=media-libs/libpng-1.6.16
         >=net-dns/libidn-1.29
         >=sys-libs/glibc-2.20
         >=sys-libs/zlib-1.2.8
        "

DOCS=( LICENSE README )

src_unpack() {
    default
    if use x86; then
        S=${WORKDIR}/${PN}-${PV}-linux-generic-i686
    elif use amd64; then
        S=${WORKDIR}/${PN}-${PV}-linux-generic-x86_64
    else
        die
    fi
}

src_install() {
    exeinto /opt/bin
    doexe "${FILESDIR}"/prince || die

    insinto /opt/${PN}
    doins -r "${S}"/lib/"${PN}"/* || die

    into /opt
    fperms "0755" "${DESTTREE}/opt/${PN}/bin/${PN}" || die

    einstalldocs
}
