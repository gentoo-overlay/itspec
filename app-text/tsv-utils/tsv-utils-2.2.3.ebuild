# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Command line utilities for tabular data files"
HOMEPAGE="https://github.com/eBay/tsv-utils"

inherit bash-completion-r1

if [[ ${PV} == 9999 ]]; then
        inherit git-r3
        EGIT_REPO_URI="https://github.com/eBay/tsv-utils.git"
        EGIT_BRANCH="master"
else
        SRC_URI="https://github.com/eBay/tsv-utils/archive/refs/tags/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
        KEYWORDS="~amd64 ~x86"
fi

LICENSE="Boost-1.0"
SLOT="0"
IUSE="doc extras"

RDEPEND="dev-lang/dmd"
DEPEND="${RDEPEND}"

DOCS=( docs/. )

src_install() {
	dobin bin/*
	newbashcomp bash_completion/tsv-utils tsv-utils

	if use doc; then
		einstalldocs
	fi

	if use extras; then
		dobin extras/scripts/*
	fi
}
