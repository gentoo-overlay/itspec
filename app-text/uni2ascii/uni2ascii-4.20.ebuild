# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Provides conversion in both directions between UTF-8 Unicode and many 7-bit ASCII equivalents"
HOMEPAGE="http://billposer.org/Software/uni2ascii.html"
SRC_URI="http://billposer.org/Software/Downloads/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="-tk"

DEPEND=""
RDEPEND="tk? (
	dev-lang/tcl
	dev-lang/tk
	dev-tcltk/tablelist
)"

PATCHES=("${FILESDIR}/uni2ascii-4.20-compile.patch")

src_install() {
	# https://bugs.gentoo.org/870412
	emake DESTDIR="${D}" install
	if ! use tk; then
		rm -f "${D}/usr/bin/u2a"
	fi
}
