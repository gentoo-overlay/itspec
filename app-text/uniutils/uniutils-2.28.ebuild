# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="This package consists of a set of programs for manipulating and analyzing Unicode text"
HOMEPAGE="https://www.billposer.org/Software/unidesc.html"
SRC_URI="http://billposer.org/Software/Downloads/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND=""

PATCHES=("${FILESDIR}/uniutils-2.28-compile.patch")

src_install() {
	emake DESTDIR="${D}" install
}
