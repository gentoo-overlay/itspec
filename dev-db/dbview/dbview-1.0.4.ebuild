# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Dbview - view dBase files"
HOMEPAGE="http://www.infodrom.org/projects/dbview/"
SRC_URI="http://www.infodrom.org/projects/dbview/download/${P}.tar.gz"
LICENSE=""

SLOT="0"

KEYWORDS="~x86"

IUSE=""

RESTRICT="mirror"

DEPEND=""
RDEPEND="${DEPEND}"

DOCS=( CHANGES dBASE README )

src_install() {
	emake -j1 prefix="${D}/usr" install || die "install failed"
	einstalldocs
}
