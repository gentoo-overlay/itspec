# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#inherit eutils games flag-o-matic toolchain-funcs
inherit flag-o-matic

DESCRIPTION="a frog, and a platform game"
HOMEPAGE="http://frogatto.com/"
SRC_URI="http://github.com/${PN}/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3 free-noncomm"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

IUSE_MODULES="aritest cube_trains elisa hex secluded-isle tictactoe vgi wip_levels cellular duxduo frogatto rpg stellar_limit tileciv weregild"

DEPEND="dev-libs/boost
        media-libs/glew
        media-libs/libpng:0
        media-libs/libsdl[X,audio,joystick,opengl,video]
        media-libs/sdl-image[png]
        media-libs/sdl-mixer[vorbis]
        media-libs/sdl-ttf
        sys-libs/zlib
        virtual/opengl
        virtual/glu"

RDEPEND="${DEPEND}"

for module in ${IUSE_MODULES} ; do
    IUSE+=" frogatto_modules_${module}"
done
unset module

src_prepare() {
    epatch "${FILESDIR}"/${P}-libpng.patch || die
}

src_compile() {
    emake USE_CCACHE="no" || die
}

src_install() {
    newbin "${FILESDIR}/${PN}-${PV}" "${PN}"

    insinto "/usr/share/${PN}"
    doins -r data images music *.ttf || die

    insinto "/usr/share/${PN}/modules"
    doins -r modules/frogatto || die

    for x in ${IUSE_MODULES} ; do
        if has ${x} ${FROGATTO_MODULES}; then
            doins -r modules/${x} || die
        fi
    done

    exeinto "/usr/share/${PN}"
    doexe game
    newicon images/window-icon.png "${PN}.png"
    make_desktop_entry "${PN}" "Frogatto and Friends"
}

pkg_postinst() {
    elog
    elog "To install addition modules you must add next changes to /etc/make.conf:"
    elog "1. add line"
    elog "\`USE_EXPAND=\"\${USE_EXPAND} FROGATTO_MODULES\"\`"
    elog
    elog "2. define list of required modules in FROGATTO_MODULES variable like this:"
    elog "\`FROGATTO_MODULES=\"cube_trains duxduo\"\`"
    elog
}
