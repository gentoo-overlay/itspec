# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 flag-o-matic

DESCRIPTION="a frog, and a platform game"
HOMEPAGE="http://frogatto.com/"
EGIT_REPO_URI="https://github.com/frogatto/frogatto.git"

LICENSE="CC-BY-3.0 CC-BY-NC-SA-3.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="games-engines/anura"

RDEPEND="${DEPEND}"

src_unpack() {
    git-r3_src_unpack
}

src_compile() {
    einfo "do nothing"
}

src_install() {
    newbin "${FILESDIR}/${PN}-${PV}" "${PN}"
    dodoc CHANGELOG INSTALL LICENSE README
    insinto "/usr/share/anura/modules/${PN}"
    doins -r data images locale music music_aac po sounds sounds_wav utils *.cfg || die
    newicon images/window-icon.png "${PN}.png"
    make_desktop_entry "${PN}" "Frogatto and Friends"
}
