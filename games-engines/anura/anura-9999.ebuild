# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#inherit eutils git-r3 games flag-o-matic
inherit git-r3 flag-o-matic

DESCRIPTION="Anura is the tech behind the spectacular [Frogatto & Friends]"
HOMEPAGE="http://frogatto.com/"
EGIT_REPO_URI="https://github.com/anura-engine/anura.git"
EGIT_BRANCH="trunk"

EGIT_SUBMODULES=( 'imgui' )

LICENSE="GPL-2 free-noncomm"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

IUSE_MODULES="gui icons lib_2d tbs"

DEPEND="dev-libs/boost
        media-libs/glew
        >=media-libs/glm-0.9.3.2
        media-libs/libpng:0
        media-libs/libsdl2[X,sound,joystick,opengl,video]
        media-libs/sdl2-image[png]
        media-libs/sdl2-mixer[vorbis]
        media-libs/sdl2-ttf
        sys-libs/zlib
        virtual/opengl
        virtual/glu"

RDEPEND="${DEPEND}"

for module in ${IUSE_MODULES} ; do
    IUSE+=" anura_modules_${module}"
done
unset module

src_unpack() {
    git-r3_src_unpack
}

src_compile() {
    #emake USE_CCACHE="no" || die
    # https://github.com/anura-engine/anura/issues/293
    local myemakeargs=(
        USE_CCACHE="no"
        BASE_CXXFLAGS="-Wno-literal-suffix -Wno-sign-compare"
    )
    emake "${myemakeargs[@]}" || die
}

src_install() {
    dodoc INSTALL LICENSE README.md

    insinto "/usr/share/${PN}"
    doins -r data images music || die

    insinto "/usr/share/${PN}/modules"
    for x in ${IUSE_MODULES} ; do
        if has ${x} ${ANURA_MODULES}; then
            doins -r modules/${x} || die
        fi
    done

    exeinto "/usr/share/${PN}"
    doexe ${PN}
}

pkg_postinst() {
    elog
    elog "To install addition modules you must add next changes to /etc/make.conf:"
    elog "1. add line"
    elog "\`USE_EXPAND=\"\${USE_EXPAND} ANURA_MODULES\"\`"
    elog
    elog "2. define list of required modules in ANURA_MODULES variable like this:"
    elog "\`ANURA_MODULES=\"gui icons lib_2d tbs\"\`"
    elog
}
