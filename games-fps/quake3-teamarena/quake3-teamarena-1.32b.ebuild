# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
CDROM_OPTIONAL="yes"
inherit unpacker cdrom

DESCRIPTION="Quake III Team Arena - data portion"
HOMEPAGE="http://icculus.org/quake3/"
SRC_URI="mirror://idsoftware/quake3/linux/linuxq3apoint-${PV}-3.x86.run"

LICENSE="Q3AEULA"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~x86-fbsd"
IUSE=""
RESTRICT="bindist"

RDEPEND="games-fps/quake3"
DEPEND=""

S=${WORKDIR}
GAMES_DATADIR=/usr/share/games

src_unpack() {
	use cdinstall && cdrom_get_cds Setup/missionpack/PAK0.PK3
	unpack_makeself
}

src_install() {
	einfo "Copying Team Arena files from linux client ..."
	insinto "${GAMES_DATADIR}"/quake3/missionpack
	doins missionpack/*.pk3

	if use cdinstall ; then
		einfo "Copying files from CD ..."
		newins "${CDROM_ROOT}/Setup/missionpack/PAK0.PK3" pak0.pk3
		eend 0
	fi

	find "${D}" -exec touch '{}' \;
}

pkg_postinst() {
	if ! use cdinstall ; then
		elog "You need to copy PAK0.PK3 from your Team Arena CD into"
		elog "${GAMES_DATADIR}/quake3/missionpack and name it pak0.pk3."
		elog "Or if you have got a Window installation of Q3 make a symlink to save space."
		elog
		elog "Or, re-emerge quake3-teamarena with USE=cdinstall."
		echo
	fi
}
