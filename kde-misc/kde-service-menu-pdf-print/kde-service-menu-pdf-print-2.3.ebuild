# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_P="${PN}_${PV}_all"

SRC_URI=""
KEYWORDS=""

DESCRIPTION="KDE 5 service menus for PDF documents printing"
HOMEPAGE="https://store.kde.org/p/1227799"

LICENSE="GPL-3+"
SLOT="0"
IUSE=""

DEPEND="
	!kde-misc/kde-service-menu-pdf
	kde-frameworks/plasma:5
	kde-apps/kdialog:5
"
RDEPEND="${DEPEND}"

S=${WORKDIR}

src_install() {
	dobin ${FILESDIR}/pdfprint-kdialog
	insinto /usr/share/kservices5/ServiceMenus
	doins ${FILESDIR}/pdfprint.desktop
}
