# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_P="${PN}_${PV}_all"

SRC_URI=""
KEYWORDS=""

DESCRIPTION="Various KDE 5 service menus for PDF documents"
HOMEPAGE="https://store.kde.org/p/1227799"

LICENSE="GPL-3+"
SLOT="0"
IUSE=""

DEPEND="
	kde-frameworks/plasma:5
	kde-apps/kdialog:5
	app-text/texlive-core
	dev-texlive/texlive-latexextra
"
RDEPEND="${DEPEND}"

DOCS=( doc/. )

S=${WORKDIR}/${MY_P}

src_unpack() {
	unpack "${FILESDIR}/${MY_P}.tar.gz"
}

src_prepare() {
	sed -e "s:which pdfbook:which pdfbook2:" -i "${S}"/bin/pdfbook-kdialog
	default
}

src_install() {
	einstalldocs
	dobin bin/*kdialog
	insinto /usr/share/kservices5/ServiceMenus
	doins ServiceMenus/*desktop
}
