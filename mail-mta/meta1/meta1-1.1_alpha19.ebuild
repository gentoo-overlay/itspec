# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit db-use autotools flag-o-matic

DESCRIPTION="MeTA1 is a Message Transfer Agent (MTA)"
HOMEPAGE="http://www.MeTA1.org/"

META1_CN="baddns" # Code Name for this revision
META1_PR="0"

MY_P="${PN}-${PV}"
MY_P="${MY_P/_/.}.${META1_PR}"
MY_P="${MY_P/alpha/Alpha}"

SRC_URI="http://www.meta1.org/download/.alpha/${META1_CN}/${MY_P}.tar.gz"

LICENSE="Sendmail"
RESTRICT="mirror"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="system-berkdb +msp +tls +sasl +pmilter +tinycdb dkim test timing ipv6 pinning"

DEPEND="
    acct-user/meta1
    acct-user/meta1c
    acct-user/meta1m
    acct-user/meta1q
    acct-user/meta1s
    acct-group/meta1
    acct-group/meta1c
    acct-group/meta1m
    acct-group/meta1q
    acct-group/meta1s
    system-berkdb? ( >=sys-libs/db-4.1 )
    tls? ( dev-libs/openssl )
    sasl? ( >=dev-libs/cyrus-sasl-2.1.18 )
    tinycdb? ( >=dev-db/tinycdb-0.75 )
    sys-devel/m4
    "

RDEPEND="${DEPEND}
    net-mail/mailbase
    !net-mail/vacation
    msp? ( !net-mail/mailwrapper )
    !mail-mta/courier
    !mail-mta/esmtp
    !mail-mta/exim
    !mail-mta/mini-qmail
    !mail-mta/msmtp
    !mail-mta/nbsmtp
    !mail-mta/netqmail
    !mail-mta/nullmailer
    !mail-mta/postfix
    !mail-mta/qmail-ldap
    !mail-mta/sendmail
    !mail-mta/ssmtp
    "

#PROVIDE="virtual/mta"

QA_EXECSTACK="usr/libexec/smtps usr/libexec/smtpc"

S=${WORKDIR}/${MY_P}

src_prepare() {
    default
    if use system-berkdb; then
        #eapply "${FILESDIR}"/meta1-add-db47-support-1.patch || die
        eapply "${FILESDIR}"/meta1-add-db47-support-2.patch || die
        eapply "${FILESDIR}"/meta1-add-db47-support-3.patch || die
        eapply "${FILESDIR}"/meta1-add-db47-support-4.patch || die
        #eapply "${FILESDIR}"/meta1-add-db47-support-5.patch || die
    fi

    # disable run sm.setup.sh at installation
    # due to runas failed in sandbox
    # sm.setup.sh must be runing by "ebuild ${PF}.ebuild config"
    eapply "${FILESDIR}"/meta1-disable-sm.setup.sh.patch || die

    # Regression in MeTA1 1.1.Alpha19.0
    # http://www.meta1.org/MeTA1-1.1.Alpha19.0.html#moreaddrPatch
    # http://www.meta1.org/download/.alpha/baddns/more_addr.p2
    eapply "${FILESDIR}"/more_addr.p2 || die

}

src_configure() {
    local bdbconf=""

    if use ipv6; then
        append-cflags -DMTA_NETINET6=2
    fi

    if use timing; then
        append-cflags -DSC_STATS
    fi

    if use pinning; then
        append-cflags -DFFR_CERT_PINNING
    fi

    if use system-berkdb; then
        bdbconf+="
                --disable-included-bdb \
                --with-bdb-libdir=/usr/$(get_libdir) \
                --with-bdb-incdir=$(db_includedir) \
                "
    fi

    econf \
        $(use_enable msp) \
        $(use_enable tls TLS) \
        $(use_enable sasl SASL) \
        $(use_enable pmilter) \
        $(use_enable tinycdb) \
        $(use_enable dkim DKIM) \
        ${bdbconf} \
        || die "econf failed"
}

src_compile() {
    emake -j1 || die "compile failed"
}

src_test() {
    emake -j1 check
}

src_install() {
    emake -j1 DESTDIR="${D}" install

    # configs
    insinto /etc/${PN}

    newins ${FILESDIR}/meta1-1.0.conf meta1.conf
    fowners meta1:meta1 /etc/${PN}/meta1.conf

    #doins ${FILESDIR}/mt
    #fowners meta1m:meta1m /etc/${PN}/mt

    # configurations script and helpers binary
    exeinto /usr/libexec/meta1/checks
    doexe ${S}/checks/t-getgroup || die "Installing t-getgroup failed"

    exeinto /usr/libexec/meta1/libconf
    doexe ${S}/libconf/tree || die "Installing tree failed"

    exeinto /usr/libexec/meta1/libmta
    doexe ${S}/libmta/t-hostname || die "Installing t-hostname failed"
    doexe ${S}/libmta/statit || die "Installing statit failed"

    exeinto /usr/libexec/meta1/libcheck
    doexe ${S}/libcheck/noroot || die "Installing noroot failed"

    exeinto /usr/libexec/meta1/misc
    #doexe ${S}/misc/runas || die "Installing runas failed"
    dosym /usr/bin/runas /usr/libexec/meta1/misc/runas || die "Creating symlink to runas failed"
    doexe ${S}/misc/sm.setup.sh || die "Installing sm.setup.sh failed"
    doexe ${S}/misc/sm.check.sh || die "Installing sm.check.sh failed"

    # initscript
    newinitd "${FILESDIR}"/mcp.init mcp

    dodoc -r ${S}/doc/* || die "Installing docs failed"
    docompress -x /usr/share/doc/${PF}
}

pkg_postinst() {
    elog
    elog "For initial configuration please execute"
    elog "\`${DESTTREE}/libexec/meta1/misc/sm.setup.sh\`"
    elog
    elog "To start the MeTA1 by default you should add 'mcp' to the default runlevel:"
    elog "\`rc-update add mcp default\`"
    elog
}

pkg_config() {
    if test -f ${DESTTREE}/libexec/meta1/misc/sm.setup.sh
    then
        einfo "For postinstall configuration please execute"
        einfo  "\`${DESTTREE}/libexec/meta1/misc/sm.setup.sh\`"
    else
        einfo "\`${DESTTREE}/libexec/meta1/misc/sm.setup.sh\` not exist. Please reemerge meta1"
    fi
}
