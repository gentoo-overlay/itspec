# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

FONT_PN="urwfonts"
FONT_SUFFIX="ttf"
GPDL_VER="9.54.0"
S="${WORKDIR}"/ghostpdl-"${GPDL_VER}"/pcl/urwfonts
FONT_S="${S}"

inherit font

DESCRIPTION="URW TrueType fonts"
HOMEPAGE="http://www.ghostscript.com/GhostPCL.html"
SRC_URI="https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs${GPDL_VER//.}/ghostpdl-${GPDL_VER}.tar.xz"

LICENSE="Aladdin"
RESTRICT="mirror"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
