# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="A font manager"
HOMEPAGE="http://fontmatrix.be/"

LICENSE="GPL-2"
SLOT="0"
IUSE="debug"

if [[ ${PV} == "9999" ]] ; then
    inherit git-r3
    EGIT_REPO_URI="https://github.com/fontmatrix/fontmatrix.git"
    EGIT_BRANCH="master"
    KEYWORDS=""
else
    SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${PV}.tar.gz"
    KEYWORDS="~amd64 ~x86"
    S="${WORKDIR}/${PN}-${PV}"
fi

RDEPEND="dev-qt/qtgui:5
        dev-qt/qtsql:5
        dev-qt/qtsvg:5
        dev-qt/qtwebkit:5
        media-libs/freetype:2"
DEPEND="${RPEDEND}"

DOCS=( README.md )

src_configure() {
    local mycmakeargs=()

    if use debug; then
        CMAKE_BUILD_TYPE=Debug
    fi

    mycmakeargs+=( -DOWN_SHAPER=1 )

    cmake_src_configure
}

src_install() {
    dobin "${BUILD_DIR}"/src/${PN}
    doman ${PN}.1
    domenu ${PN}.desktop
    doicon ${PN}.png
}
