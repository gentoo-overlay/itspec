# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop qmake-utils git-r3

DESCRIPTION="PosteRazor cuts raster images into multipage PDF documents."
HOMEPAGE="http://posterazor.sourceforge.net/"

EGIT_REPO_URI="https://github.com/aportale/posterazor.git"
EGIT_BRANCH="master"

LICENSE="GPL-3"
SLOT="0"
IUSE="qt5 qt6"
REQUIRED_USE="|| ( qt5 qt6 )"

RDEPEND="
	!qt6? (
		dev-qt/qtgui:5
		dev-qt/qtcore:5
		dev-qt/qtxml:5
		dev-qt/qtwidgets:5
		dev-qt/qtprintsupport:5
		app-text/poppler[qt5]
	)
	qt6? (
		dev-qt/qtbase:6[gui,widgets,xml]
		app-text/poppler[qt6]
	)

"
DEPEND="${RDEPEND}"

PATCHES=( "${FILESDIR}/${PN}-qt-5-build-fix.patch" )

DOCS=( CHANGES README )

S=${WORKDIR}/${P}

src_prepare() {
	if use qt6; then
		eapply "${FILESDIR}/${PN}-qt-6-build-fix.patch"
	fi
	eapply_user
}

src_configure() {
	if use qt5; then
		eqmake5 PREFIX="${D}"/usr -r src/posterazor.pro
	elif use qt6; then
		eqmake6 PREFIX="${D}"/usr -r src/posterazor.pro
	fi
}

src_install() {
	exeinto /usr/bin
	doexe ${S}/PosteRazor
	einstalldocs
	newicon ${S}/src/posterazor.png ${PN}.png
	make_desktop_entry PosteRazor "PosteRazor" /usr/share/pixmaps/${PN}.png
}
