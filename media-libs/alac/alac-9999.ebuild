# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3

DESCRIPTION="Apple Lossless Audio Codec"
HOMEPAGE="http://alac.macosforge.org/"
EGIT_REPO_URI="https://github.com/macosforge/alac.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

DOCS=( ALACMagicCookieDescription.txt ReadMe.txt )

src_compile() {
    emake  -C ${S}/codec || die "compile failed"
}

src_install() {
    dolib.a ${S}/codec/libalac.a || die "install failed"
    insinto /usr/include/alac
    doins ${S}/codec/*.h || die "install headers failed"
    einstalldocs
}
