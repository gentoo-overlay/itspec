# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3

DESCRIPTION=" ALAC CAF/WAVE convert command line utility"
HOMEPAGE="http://alac.macosforge.org/"
EGIT_REPO_URI="https://github.com/macosforge/alac.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="media-libs/alac"
RDEPEND="${DEPEND}"

PATCHES=( "${FILESDIR}/alacconvert-makefile.patch" )

src_compile() {
    emake -C ${S}/convert-utility || die "compile failed"
}

src_install() {
    dobin ${S}/convert-utility/alacconvert || die "install failed"
}
