# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit udev

DESCRIPTION="User-space driver for USB-enabled Olympus DVRs that do not support the USB Mass Storage."
HOMEPAGE="http://code.google.com/p/odvr/"
SRC_URI="https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/${PN}/${PN}-${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
RESTRICT="mirror"

KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="
        virtual/libusb
        media-libs/libsndfile"

RDEPEND="
        virtual/udev
        virtual/libusb
        dev-libs/libusb-compat
        media-libs/libsndfile"



# patch from http://code.google.com/p/odvr/issues/detail?id=6
PATCHES=( "${FILESDIR}/add_pulcod2_support_to_olympusdrv.patch" )

src_prepare() {
    default

    # In newer versions of udev the SYSFS field has changed to ATTR
    sed -e "s:SYSFS:ATTR:g" -i "${S}/41-odvr.rules" || die "patching 41-odvr.rules failed"
}

src_compile() {
    emake || die "emake failed"
}

src_install() {
    dobin odvr
    dobin odvr-gui
    insinto /lib/udev/rules.d
    doins 41-odvr.rules
    dodoc README
}

pkg_postinst() {
    if [[ -z "${REPLACING_VERSIONS}" ]]; then
        udev_reload
    fi
}

pkg_postrm() {
    if [[ -z "${REPLACING_VERSIONS}" ]]; then
        udev_reload
    fi
}
