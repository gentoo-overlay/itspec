# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 udev

DESCRIPTION="User-space driver for USB-enabled Olympus DVRs that do not support the USB Mass Storage."

HOMEPAGE="https://github.com/twilly/odvr"
EGIT_REPO_URI="https://github.com/twilly/odvr.git"
EGIT_BRANCH="master"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
RESTRICT="mirror"

KEYWORDS=""
IUSE=""

DEPEND="
        virtual/libusb
        media-libs/libsndfile"

RDEPEND="
        virtual/udev
        virtual/libusb
        dev-libs/libusb-compat
        media-libs/libsndfile"

src_install() {
    dobin odvr
    dobin odvr-gui
    insinto /lib/udev/rules.d
    doins 41-odvr.rules
    dodoc README
}

pkg_postinst() {
    elog ""
    elog "You will need to reload udev rules prior running ${PN}:"
    elog "# udevadm control --reload-rules"
    elog ""
}

pkg_postinst() {
    if [[ -z "${REPLACING_VERSIONS}" ]]; then
        udev_reload
    fi
}

pkg_postrm() {
    if [[ -z "${REPLACING_VERSIONS}" ]]; then
        udev_reload
    fi
}
