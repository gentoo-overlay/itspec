# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="MPlayer/MPV IPTV script"
HOMEPAGE=""
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~x86-fbsd ~amd64-fbsd"
IUSE="mplayer +mpv"
REQUIRED_USE="|| ( mplayer mpv )"

RDEPEND="mplayer? ( media-video/mplayer[network] )
         mpv? ( media-video/mpv )
         app-text/dos2unix
         >=x11-misc/rofi-1.5
         "

DEPEND=${RDEPEND}

S=${FILESDIR}

src_install() {
    newbin ${FILESDIR}/mplayer-iptv-${PV} mplayer-iptv
    newbin ${FILESDIR}/rofi-iptv-menu-${PV} rofi-iptv-menu

    newconfd "${FILESDIR}"/mplayer-iptv-${PV}.conf mplayer-iptv

    # icon 48x48 from http://commons.wikimedia.org/wiki/File:Applications-mplayer.svg
    # Author: Gauthier Tellier
    newicon ${FILESDIR}/Applications-mplayer.svg mplayer-iptv.svg

    insinto /usr/share/applications
    newins mplayer-iptv.desktop mplayer-iptv.desktop

    insinto /usr/share/rofi/themes
    newins bw.rasi bw.rasi
}

pkg_postinst() {
    elog "\nDefault configuration file placed to /etc/conf.d/mplayer-iptv"
    elog "At first run configuration file will be copied to \${XDG_CONFIG_HOME}/mplayer-iptv.conf"
    elog "Please define PLAYLIST_URL, MPLAYER_CMD, MPLAYER_OPT and MENU_CMD variables in configuration file\n"
}
