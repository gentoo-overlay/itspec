# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit flag-o-matic git-r3

DESCRIPTION="Powerful and easy-to-use multimedia player"
HOMEPAGE="http://bomi-player.github.io/"
# forked from xylosper/bomi "https://github.com/xylosper/bomi.git"
EGIT_REPO_URI="https://github.com/d-s-x/bomi.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="aacs bdplus cdda jack pulseaudio samba systemd +vaapi +vdpau youtube-dl"

RDEPEND="dev-libs/fribidi
	dev-libs/icu
	dev-libs/libchardet
	>=dev-qt/qtcore-5.3
	>=dev-qt/qtdbus-5.3
	>=dev-qt/qtdeclarative-5.3
	>=dev-qt/qtgui-5.3
	>=dev-qt/qtnetwork-5.3
	>=dev-qt/qtopengl-5.3
	>=dev-qt/qtquickcontrols-5.3
	>=dev-qt/qtsql-5.3
	>=dev-qt/qtwidgets-5.3
	>=dev-qt/qtx11extras-5.3
	>=dev-qt/qtxml-5.3
	>=dev-qt/linguist-tools-5.3
	>=dev-qt/qtsvg-5.3
	media-libs/alsa-lib
	>=media-video/ffmpeg-2.4
	media-libs/glew
	>=media-libs/libass-0.12.1
	youtube-dl? ( net-misc/youtube-dl )
	media-libs/libbluray
	bdplus? ( media-libs/libbdplus )
	aacs? ( media-libs/libaacs )
	>=media-libs/libquvi-0.9
	media-libs/libdvdread
	media-libs/libdvdnav
	media-sound/mpg123
	vaapi? ( media-libs/libva[opengl] )
	vdpau? ( x11-libs/libvdpau )
	cdda? (
		dev-libs/libcdio
		dev-libs/libcdio-paranoia )
	jack? ( media-sound/jack-audio-connection-kit )
	pulseaudio? ( media-sound/pulseaudio )
	samba? ( net-fs/samba[client] )
	systemd? ( sys-apps/systemd )
	virtual/opengl"
DEPEND="${RDEPEND}
	dev-lang/python
	media-libs/mesa
	sys-apps/sed
	>=sys-devel/gcc-4.9"

DOCS="CHANGES.txt README.md"

src_prepare() {
	eapply "${FILESDIR}"/bomi-configure.patch
	eapply "${FILESDIR}"/bomi-declaration.patch
	eapply "${FILESDIR}"/bomi-glibc-include.patch

	if [ $(get_libdir) = "lib64" ]; then
		eapply "${FILESDIR}"/bomi-makefile.patch
		eapply "${FILESDIR}"/bomi-configure.pro.patch
	fi

	eapply_user
}

src_configure() {

	# Require C++14 explicitly, commit/5ec488bca62ea5ae37784f8415959622a32e2de2
	append-cxxflags "-std=c++14 -Wl,-rpath=${WORKDIR}/build/$(get_libdir)"

	./configure \
		--qtsdk=/usr/$(get_libdir)/qt5 \
		--qmake=/usr/$(get_libdir)/qt5/bin/qmake \
		--prefix="${EPREFIX}"/usr \
		$(use_enable cdda) \
		$(use_enable jack) \
		$(use_enable pulseaudio) \
		$(use_enable samba) \
		$(use_enable systemd) \
		$(use_enable vaapi) \
		$(use_enable vdpau) \
		|| die
}

src_install() {
	emake DEST_DIR="${D}" install
	einstalldocs
}
