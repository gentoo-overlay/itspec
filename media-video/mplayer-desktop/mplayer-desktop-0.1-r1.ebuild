# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit fdo-mime

DESCRIPTION="Mplayer desktop integration"
HOMEPAGE=""
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~x86-fbsd ~amd64-fbsd"
IUSE="kde kdialog"

RDEPEND="media-video/mplayer
        kdialog? ( kde-apps/kdialog )
        "

DEPEND=${RDEPEND}

S=${FILESDIR}

src_install() {
    # icon 48x48 from http://commons.wikimedia.org/wiki/File:Applications-mplayer.svg
    # Author: Gauthier Tellier
    #insinto /usr/share/icons/hicolor/scalable/apps
    #newins Applications-mplayer.svg mplayer.svg
    newicon ${FILESDIR}/Applications-mplayer.svg mplayer.svg || die

    # Open With...
    insinto /usr/share/applications
    newins mplayer-applications.desktop mplayer.desktop

    # KDE: Menu, Desktop
    if use kdialog; then
        newins mplayer-kdialog-openfile.desktop mplayer-openfile.desktop
    fi

    # KDE: Service Menu (Actions entry of the context menu)
    if use kde; then
        #insinto /usr/share/kde4/services/ServiceMenus
        insinto /usr/share/kservices5/ServiceMenus/
        newins mplayer-services.desktop mplayer.desktop || die
        newins mplayer-dvd-services.desktop mplayer-dvd.desktop || die
    fi


}

pkg_postinst() {
    fdo-mime_mime_database_update
    fdo-mime_desktop_database_update
    elog "\nRun \`xdg-mime default mplayer.desktop \"video/mp4\"\` as regular user"
    elog "to make MPlayer default media player for mp4 video."
}

pkg_postrm() {
    fdo-mime_mime_database_update
    fdo-mime_desktop_database_update
}
