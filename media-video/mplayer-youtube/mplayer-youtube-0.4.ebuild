# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Simple Youtube Video player using MPlayer/MPV and youtube-dl in shell script"
HOMEPAGE=""
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~x86-fbsd ~amd64-fbsd"
IUSE="mplayer +mpv"
REQUIRED_USE="|| ( mplayer mpv )"

RDEPEND="mplayer? ( media-video/mplayer[network] )
         mpv? ( media-video/mpv )
         media-video/ffmpeg[gnutls,openssl]
         net-misc/yt-dlp
         x11-misc/xclip"

DEPEND=${RDEPEND}

S=${FILESDIR}

src_install() {
    newbin ${FILESDIR}/mplayer-youtube-${PV} mplayer-youtube
    newconfd "${FILESDIR}"/mplayer-youtube-${PV}.conf mplayer-youtube
}

pkg_postinst() {
    elog "\nDefault configuration file placed to /etc/conf.d/mplayer-youtube"
    elog "At first run configuration file will be copied to \${XDG_CONFIG_HOME}/mplayer-youtube.conf"
    elog "Please define MPLAYER_CMD and YOUTUBE_DL_FORMAT variables in configuration file"
}
