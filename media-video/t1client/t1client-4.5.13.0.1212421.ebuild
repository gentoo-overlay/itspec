# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop

DESCRIPTION="TRASSIR Client for Linux"
HOMEPAGE="https://confluence.trassir.com/pages/viewpage.action?pageId=36865118"
SRC_URI="https://ncloud.dssl.ru/s/spbaAkxjPKR4C5L/download/t1client-4.5.13.0-1212421-Release.tar"

SLOT="0"
KEYWORDS="~amd64 ~x86"
RESTRICT="strip"

DEPEND="
	sys-libs/libcap
	media-libs/glu
"

RDEPEND="${DEPEND}"

S=${WORKDIR}

src_prepare() {
	default
	${S}/opt/dssl/t1client/install.sh
}

src_install() {
	insinto /opt/dssl/${PN}
	doins -r ${S}/opt/dssl/t1client/* || die
	exeinto /opt/bin
	doexe "${FILESDIR}/${PN}" || die
	fperms 777 /opt/dssl/t1client/run_t1client.sh /opt/dssl/t1client/t1client-core2
	make_desktop_entry ${PN} "TRASSIR Client" ${PN} "Video" || die
}
