# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools git-r3

DESCRIPTION="NetXMS - network and infrastructure monitoring and management system"
HOMEPAGE="https://netxms.org/"
EGIT_REPO_URI="https://github.com/netxms/netxms.git"
EGIT_COMMIT="release-${PV}"
SRC_URI=""

LICENSE="LGPL GPL BSD"

SLOT="0"

KEYWORDS="~x86 ~amd64"

# database driver: sqlite odbc oracle mysql mariadb mariadb-compat-headers pgsql db2 informix
IUSE="agent client server flow-collector sqlite mysql mariadb pgsql"
REQUIRED_USE="|| ( agent client server ) ( agent? ( || ( sqlite mysql mariadb pgsql ) ) )"

RESTRICT="mirror"

DEPEND="dev-libs/libpcre[pcre32]
	sqlite? ( dev-db/sqlite )
	mysql? ( dev-db/mysql )
	mariadb? ( dev-db/mariadb )
	pgsql? ( dev-db/postgresql )"

RDEPEND="${DEPEND}"

DOCS=( AUTHORS COPYING ChangeLog NEWS README README.md THANKS UPGRADE )

S=${WORKDIR}/${PN}-${PV}

src_prepare() {
	default
	${S}/init-source-tree
	eautoreconf
}

src_configure() {
	econf \
		$(use_with agent) \
		$(use_with client) \
		$(use_with server) \
		$(use_with flow-collector) \
		$(use_with sqlite) \
		$(use_with mysql) \
		$(use_with mariadb) \
		$(use_with pgsql)
}

src_install() {
	default
	einstalldocs

	if use server; then
		newconfd ${FILESDIR}/netxmsd.confd netxmsd
		newinitd ${FILESDIR}/netxmsd.initd netxmsd
		insinto /etc
		newins ${FILESDIR}/netxmsd.conf netxmsd.conf
		keepdir /var/log/netxms
	fi

	if use agent; then
		newconfd ${FILESDIR}/nxagentd.confd nxagentd
		newinitd ${FILESDIR}/nxagentd.initd nxagentd
		insinto /etc
		newins ${FILESDIR}/nxagentd.conf nxagentd.conf
		keepdir /etc/nxagentd.conf.d
		keepdir /var/log/netxms
	fi
}
