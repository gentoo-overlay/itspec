# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Advanced CLI tool for sending email"
HOMEPAGE="https://github.com/deanproxy/eMail"
SRC_URI=""
EGIT_REPO_URI="https://github.com/deanproxy/eMail.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~x86"

PATCHES=(
	"${FILESDIR}"/${PN}-${PV}-fno-common.patch
	"${FILESDIR}"/${PN}-${PV}-fix-clang16-configure.patch
	"${FILESDIR}"/${PN}-${PV}-fix-tls.patch
)

src_install() {
	default
	doman email.1
	dodoc README.md TODO RFC821 VERSION rfc3156-openpgp.txt
}
