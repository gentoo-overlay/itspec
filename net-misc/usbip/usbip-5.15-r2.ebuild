# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ETYPE="sources"
K_NOUSENAME=1
inherit autotools kernel-2 systemd

DESCRIPTION="Userspace utilities for a general USB device sharing system over IP networks"
HOMEPAGE="https://www.kernel.org/"
SRC_URI="${KERNEL_URI}"
S="${WORKDIR}/linux-${PV}/tools/usb/${PN}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="tcpd systemd"

RDEPEND="
	>=dev-libs/glib-2.6
	sys-apps/hwdata
	>=sys-kernel/linux-headers-3.17
	virtual/libudev
	tcpd? ( sys-apps/tcp-wrappers )
	systemd? ( sys-apps/systemd )"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

PATCHES=( "${FILESDIR}/${PN}-vhci-bind.patch" )

src_unpack() {
	tar xJf "${DISTDIR}"/${A} linux-${PV}/tools/usb/${PN} || die
}

src_prepare() {
	default
	# remove -Werror from build, bug #545398
	sed -i 's/-Werror[^ ]* //g' configure.ac || die

	eautoreconf
}

src_configure() {
	econf \
		$(usev !tcpd --without-tcp-wrappers) \
		--with-usbids-dir="${EPREFIX}"/usr/share/hwdata
}

src_install() {
	default
	find "${ED}" -name '*.la' -delete || die
	newconfd ${FILESDIR}/usbipd.confd usbipd
	newinitd ${FILESDIR}/usbipd.initd usbipd

	if use systemd; then
		systemd_newunit ${FILESDIR}/usbipd.service usbipd.service
		systemd_newunit ${FILESDIR}/usbip-bind.service usbip-bind@.service
	fi
}

pkg_postinst() {
	elog "For using USB/IP you need to enable USBIP_VHCI_HCD in the client"
	elog "machine's kernel config and USBIP_HOST on the server."

	if use systemd; then
		elog "Enable and start the daemon with:"
		elog "systemctl enable usbipd"
		elog "systemctl start usbipd"
		elog "Then add binds with:"
		elog "systemctl start usbip-bind@1-1.2.3"
		elog "Replace \"1-1.2.3\" with the bind id of the USB device you want to share."
		elog "Find the bind ids with:"
		elog "usbip list -l"
	fi
}
