# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Driver for Canon CAPT printers for Canon LBP-810, Canon LBP-1120"

HOMEPAGE="https://www.boichat.ch/nicolas/capt/"
SRC_URI="https://www.boichat.ch/nicolas/capt/capt-0.1.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

PATCHES=("${FILESDIR}/capt-compile.patch")

DEPEND=""

RDEPEND="${DEPEND}
        net-print/cups"

DOCS="COPYING NEWS README SPECS THANKS TODO"

src_compile() {
    emake CFLAGS="${CFLAGS} -std=gnu99"
}

src_install() {

    einstalldocs

    dobin "${S}/capt" || die
    dobin "${S}/capt-print" || die

    insinto "/usr/share/cups/model/"
    doins "${S}/ppd/Canon-LBP-810-capt.ppd" || die

}
