# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3

DESCRIPTION="Driver for Canon CAPT printers"
HOMEPAGE="http://www.linux.org.ru/forum/linux-hardware/4868236#comments"
EGIT_REPO_URI="https://github.com/ra1nst0rm3d/captdriver.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""

RDEPEND="${DEPEND}
        net-print/cups
"

DOCS="AUTHORS COPYING INSTALL INSTALL.git README SPECS"

src_unpack() {
    git-r3_src_unpack
}

src_configure() {
    cd ${S}
    aclocal; autoconf; automake --add-missing
    ./configure
}

src_compile() {
    emake
}

src_install() {

    einstalldocs

    exeinto "/usr/libexec/cups/filter"
    doexe "${S}/src/rastertocapt" || die

    insinto "/usr/share/cups/model/"
    doins "${S}/Canon-LBP2900.ppd" || die
    doins "${S}/Canon-LBP3000.ppd" || die

}
