# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Pantum printer Linux driver"
HOMEPAGE="https://www.pantum.cn/support/download/driver/"

IUSE="+scanner"

SRC_URI="https://bitbucket.org/gentoo-overlay/itspec/downloads/pantum_1.1.106-1_amd64.deb"

LICENSE="AS-IS"
SLOT="0"
KEYWORDS="~amd64"

COMMON_DEPEND="
	media-libs/libjpeg8
	net-print/cups
	sys-apps/dbus
	virtual/jpeg:0
	net-print/cups-filters
	scanner? (
		media-gfx/sane-backends
	)
"
BDEPEND="
	virtual/pkgconfig
"
DEPEND="
	${COMMON_DEPEND}
"
RDEPEND="
	${COMMON_DEPEND}
	app-text/ghostscript-gpl
"

S="${WORKDIR}"

src_prepare(){
	eapply_user
	unpack "${S}/data.tar.xz"
}

src_install(){

	if use scanner ; then
		insinto /etc
		doins -r "${S}/etc/sane.d"

		insinto /usr/local/etc
		doins -r "${S}/usr/local/etc/sane.d"

		insinto /usr/lib64
		doins -r "${S}/usr/lib/x86_64-linux-gnu/sane"
	fi

	insinto /lib/udev
	doins -r "${S}/etc/udev/rules.d"

	exeinto /opt/pantum/bin
	doexe "${S}/opt/pantum/bin/ptqpdf"

	insinto /opt/pantum
	doins -r "${S}/opt/pantum/ippfilter"

	insinto /usr/libexec/cups
	insopts -m0755
	doins -r "${S}/usr/lib/cups/filter"

	insinto /usr/share/cups/model
	insopts -m0644
	doins -r "${S}/usr/share/cups/model/Pantum"
}
