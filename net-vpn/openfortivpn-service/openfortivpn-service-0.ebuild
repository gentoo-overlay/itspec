# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="openrc service for net-vpn/openfortivpn"
LICENSE=""

SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE=""

DEPEND="net-vpn/openfortivpn"
RDEPEND="${DEPEND}"

S=${WORKDIR}
src_install() {
	newconfd ${FILESDIR}/openfortivpn.confd openfortivpn
	newinitd ${FILESDIR}/openfortivpn.initd openfortivpn
}
