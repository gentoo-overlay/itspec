# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Eugene Filatov <itspec.ru@gmail.com> (2022-01-25)
# kde-service-menu-pdf-2.3 is a development release (for now).
kde-misc/kde-service-menu-pdf
