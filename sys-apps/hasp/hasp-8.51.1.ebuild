# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker linux-info udev systemd

DESCRIPTION="Sentinel LDK Linux Runtime"
HOMEPAGE="https://www.euromobile.ru/download-center/"

# *.deb unpacked from https://www.euromobile.ru/download-center/files/Sentinel_LDK_Linux_Runtime_Installer_script.tar.gz
SRC_URI="x86?   ( https://bitbucket.org/gentoo-overlay/itspec/downloads/aksusbd-${PV}-i386.deb )
         amd64? ( https://bitbucket.org/gentoo-overlay/itspec/downloads/aksusbd-${PV}-amd64.deb )
         arm?   ( https://bitbucket.org/gentoo-overlay/itspec/downloads/aksusbd-${PV}-armhf.deb )
         arm64? ( https://bitbucket.org/gentoo-overlay/itspec/downloads/aksusbd-${PV}-arm64.deb )
         "

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64"

IUSE="systemd udev"

S="${WORKDIR}"

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	dodir /etc/hasplm/templates
	insinto /etc/hasplm/templates
	doins "${S}"/etc/hasplm/templates/*.alp

	if use udev; then
		insinto $(get_udevdir)/rules.d
		doins "${S}"/etc/udev/rules.d/*.rules
	fi

	newconfd "${FILESDIR}"/hasplmd.conf hasplmd
	newinitd "${FILESDIR}"/hasplmd.init hasplmd

	dosbin "${S}"/usr/sbin/aksusbd*
	dosbin "${S}"/usr/sbin/hasplmd*

	if use systemd; then
		systemd_dounit ${S}/var/hasplm/init/*.service
	fi
}
