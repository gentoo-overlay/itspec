# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 autotools flag-o-matic

DESCRIPTION="Native C/C++ library of USB Virtual Host Controller Interface"
HOMEPAGE="https://sourceforge.net/p/usb-vhci/wiki/Home/"
EGIT_REPO_URI="https://git.code.sf.net/p/usb-vhci/libusb_vhci"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

IUSE=""

DEPEND="app-emulation/usb-vhci"

src_prepare() {
	eautoreconf
	default
}

src_configure() {
	append-cxxflags -std=c++11
	default
}
