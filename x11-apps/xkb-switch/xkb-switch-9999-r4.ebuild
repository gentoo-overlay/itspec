# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3

DESCRIPTION="Switch your X keyboard layouts from the command line"
HOMEPAGE="https://github.com/ierton/xkb-switch"
EGIT_REPO_URI="https://github.com/ierton/xkb-switch"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="x11-libs/libxkbfile"
RDEPEND="${DEPEND}"

src_prepare() {
    sed -e "s:LIBRARY DESTINATION lib OPTIONAL:LIBRARY DESTINATION lib\${LIB_SUFFIX} OPTIONAL:" -i "${S}"/CMakeLists.txt
    cmake_src_prepare
}
