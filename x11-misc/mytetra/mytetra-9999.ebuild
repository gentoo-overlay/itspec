# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils git-r3

EGIT_REPO_URI="https://github.com/xintrea/mytetra_dev.git"
EGIT_BRANCH="experimental"
#EGIT_HAS_SUBMODULES="true"

DESCRIPTION="Smart manager for information collecting"
HOMEPAGE="https://github.com/xintrea/mytetra_dev"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="dev-qt/qtgui:5
         dev-qt/qtcore:5
         dev-qt/qtxml:5
         dev-qt/qtxmlpatterns:5
         dev-qt/qtsvg:5
         dev-qt/qtnetwork:5
         dev-qt/qtwidgets:5
         dev-qt/qtprintsupport:5
         "

DEPEND="${RDEPEND}"

src_prepare() {
    default
    sed 's|/usr/local|$${PREFIX}|' -i app/app.pro
    sed 's|/usr/share|$${PREFIX}/share|' -i app/app.pro

    # fix bug https://github.com/xintrea/mytetra_dev/issues/164
    # https://github.com/NixOS/nixpkgs/pull/263371
    sed 's|const char \*strcasestr|char \*strcasestr|' -i thirdParty/mimetex/mimetex.c

}

src_configure() {
    eqmake5 PREFIX="${D}"/usr
}
