# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 qmake-utils

DESCRIPTION="Stamina - клавиатурный тренажёр для бабушек и дедушек, а также их родителей!"
HOMEPAGE="https://stamina.ru/"
EGIT_REPO_URI="https://github.com/zeal18/qstamina.git"
EGIT_BRANCH="master"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	dev-qt/qtmultimedia:5
	dev-qt/qtnetwork:5
	dev-qt/linguist-tools:5
"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}-${PV}/src"

src_prepare() {

	eapply "${FILESDIR}"/${PN}-build.patch || die
	eapply "${FILESDIR}"/${PN}-install.patch || die
	eapply "${FILESDIR}"/${PN}-translation.patch || die

	LRELEASE="$(qt5_get_bindir)/lrelease"
	LUPDATE="$(qt5_get_bindir)/lupdate"

	${LUPDATE} src.pro || die

	rm ${S}/resources/qm/*.qm
	${LRELEASE} ${S}/resources/ts/*.ts || die
	mv ${S}/resources/ts/*.qm ${S}/resources/qm/ || die

	# fix layouts location
	mkdir ${S}/resources/layouts
	mv ${S}/resources/*.ltf ${S}/resources/layouts/ || die

	# fix scalable icon location
	mkdir ${S}/resources/icons/scalable || die
	mv ${S}/resources/qstamina-icon.svg ${S}/resources/icons/scalable/qstamina.svg || die

	# fix binary filename
	sed 's|TARGET = ../QStamina|TARGET = ../qstamina|' -i ${S}/src.pro

	default
}

src_configure() {
	eqmake5 PREFIX="${EPREFIX}"/usr src.pro
}

src_install() {
	emake install INSTALL_ROOT="${D}"
}
