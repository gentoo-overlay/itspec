# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools git-r3

DESCRIPTION="A plugin for rofi that emulates blezz behaviour"
HOMEPAGE="https://github.com/davatorium/rofi-blezz"

EGIT_REPO_URI="https://github.com/davatorium/rofi-blezz.git"
RDEPEND="x11-misc/rofi"

DEPEND="${RDEPEND}"

LICENSE="MIT"
SLOT="0"

src_prepare() {
    default
    eautoreconf
}

src_install() {
    default
    newdoc Example/content content.example
}
