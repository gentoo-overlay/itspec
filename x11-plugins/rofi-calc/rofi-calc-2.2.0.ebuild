# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION="A rofi plugin that uses libqalculate's qalc to parse natural language input and provide results"
HOMEPAGE="https://github.com/svenstaro/rofi-calc"

if [[ ${PV} != *9999* ]]; then
        SRC_URI="https://github.com/svenstaro/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
        KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ppc ~ppc64 ~x86 ~amd64-linux"
else
        inherit git-r3
        SRC_URI=""
        EGIT_REPO_URI="https://github.com/svenstaro/${PN}.git"
        KEYWORDS=""
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="
	x11-misc/rofi
	>=sci-libs/libqalculate-2.0
"
RDEPEND="${DEPEND}"

DOCS=( README.md )

src_prepare() {
	default
	eautoreconf -i
}

src_install() {
	default
	find "${ED}" -name '*.la' -delete || die
}
