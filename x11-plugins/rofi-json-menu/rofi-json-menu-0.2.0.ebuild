# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3

DESCRIPTION="A plugin to use rofi for custom menus"
HOMEPAGE="https://github.com/marvinkreis/rofi-json-menu"
EGIT_REPO_URI="https://github.com/marvinkreis/${PN}.git"

if [[ ${PV} != *9999* ]]; then
	EGIT_CLONE_TYPE="single"
	EGIT_COMMIT="${PV}"
	EGIT_SUBMODULES=( submodules/libnkutils )
	KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ppc ~ppc64 ~x86 ~amd64-linux"
fi

RDEPEND="
	>=x11-misc/rofi-1.6.0
	>=dev-libs/json-c-0.13
"

DEPEND="${RDEPEND}"

LICENSE="MIT"
SLOT="0"

DOCS=( README.md )
