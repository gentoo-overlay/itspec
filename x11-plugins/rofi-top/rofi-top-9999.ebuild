# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools git-r3

DESCRIPTION="A plugin for rofi that emulates top behaviour"
HOMEPAGE="https://github.com/davatorium/rofi-top"

EGIT_REPO_URI="https://github.com/davatorium/rofi-top.git"

RDEPEND="
	gnome-base/libgtop
	x11-misc/rofi
"

DEPEND="${RDEPEND}"

LICENSE="MIT"
SLOT="0"

DOCS=( README.md )

src_prepare() {
    default
    eautoreconf
}
